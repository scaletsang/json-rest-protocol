# Sockcut (In development)

Sockcut, a shortcut for easy maintaining and scaling your web application that use socket connection or only one endpoint.
Every socket application have some sort of message protocol for communication between client and server.
With Sockcut, you write the message protocol in a json format,
give it to Sockcut, and you can have fun doing the application logic.
Now whenever you need to add or change your message types,
change it simply in the json file!
No worry about mismatched message data types or field names.
No more breaking of relationship,
long lived the romantic story between the client and the server.

## The Predicament, can be life in general? lol

Once upon a time, you started having fun with a little web socket application, and you say
"Oh, it will be sickkkkkk to add this feature, that feature, and change these features!"
The next second you realized you have to add a million different message types to communicate back and forth the client and server.
You construct a message types protocol conceptually or on a paper,
On the client side, you write a function that send out a message in the right format and data types.
On the server side, you add a case to your switch statement that receives that message type and perform some actions.
*... Not long later, the server receives an input from different users, so*
*you need to do similar things again!*
On the server side, you write a function that send out another message type to the clients, 
this function runs upon some arbitary actions happening on the server, be it an input from some other users!
On the client side, you add a case to your switch statement that receives that message type and perform some actions.
You start thinking suicidal,
where is the long lived ever after and happily forever?

## Installation (still unsure)

Run the following command

```bash
  git clone http://gitlab.com/scaletsang/socketcut
  cd sockcut
  make sockcut
```

It will ask you what language would you like your backend to be,
choose one of which. It will generate the corresponding files.

## Getting started

app.js:

```javascript
  import Sockcut from './sockcut.js'

  const sockcut = Sockcut.init("ws://example.com/my-namespace")

  sockcut.on('example_server_message_type', () => {
    // a function to run when a specific message type is received from server
  })

  // Send message to the server
  sockcut.send('example_client_message_type', data)
```

app.rb (Backend can be of other languages):

```ruby
  require 'sockcut'

  json_protocol = File.read('json_protocol.json')
  sockcut = Sockcut.init(json_protocol)

  sockcut.on 'example_client_message_type' do
    # a function to run when a message type is received from client
  end

  # Send message back to the client
  sockcut.send 'example_server_message_type', data

```

json_protocol.json
Here I defined two message types.

```javascript
{
  "example_server_message_type": {
    "field1": "str",
    "data": {
      "datafield1": "num",
      "rest": "list"
    }
  }.
  "example_client_message_type": {
    "id": "num",
    "field_example": 3
  }
}
```

## Difference between Sockcut and GraphQL

Difference between a query and Sockcut

**GraphQL**:  
client: make query -> server: receive and run query -> server: send query result -> client: receive query result  
**Sockcut**:  
client: send message -> server: receive and run functions based on the message type -> server: perform side effects  
server: send message -> client: receive and run functions based on the message type -> client: perform side effects  

Inspired by socket.io and GraphQL

## Syntax for the json Protocol

```javascript
  {
    // string type
    "TYPE": "str",
    // boolean type/ constant boolean types
    "TYPE": "bool",
    // number type/ constant number type
    "TYPE": "num",
    
    // null type
    "TPYE": null,
    // constant string type
    "TYPE": "const:STR",
    // constant boolean type
    "TYPE": true | false,
    // constant number type
    "TYPE": 0 | 1 | 2 | ...,

    // Union type
    "TYPE": ["TYPE", "TYPE", ...],

    // object of any type
    "TYPE": "obj",
    // object of a specific type
    "TYPE": {"FIELD": "TYPE"},
    // object of few specific types
    "TYPE": {"FIELD": ["TYPE", "TYPE"]},

    // list of any type
    "TYPE": "list",
    // list of any type with a specified length
    "TYPE": "list:1",
    // list of a specific type
    "TYPE": {"list": "TYPE"},
    // list of a specified type with a specified length
    "TYPE": {"list:1": "TYPE"},
    // list of few specific type
    "TYPE": {"list": ["TYPE", "TYPE"]}
  }
```