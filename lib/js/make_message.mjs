export function constructMessage(data, messageTypeNotation) {
  if (messageTypeNotation === null) return null
  const message = Object.assign({}, messageTypeNotation)
  for (const propName in message) {
    const propType = message[propName]
    /** list type */
    if (/list\:\d*/.test(propName) || propName === 'list') {
      return data.map(listItem => constructMessage(listItem, propType))
    }
    switch (typeof propType) {
      case 'boolean':
      case 'number':
        message[propName] = propType
        break
      case 'string':
        /** constant type */
        let constant = propType.match(/const\:(.*)/)
        message[propName] = constant? constant[1] : data[propName]
        break
      case 'object':
        /** union type */
        if (Array.isArray(propType)) {
          message[propName] = data[propName]
          break
        }
        /** recurse when encountered object type */
        message[propName] = constructMessage(data[propName], propType)
        break
      default:
        /** any other types */
        message[propName] = data[propName]
    }
  }
  return message
}

class Message {
  typeProtocol
  constructor(jsonProtocol) {
    this.typeProtocol = jsonProtocol
  }

  static init(jsonProtocol) {
    return new Message(Message.validateProtocol(jsonProtocol))
  }

  static validateProtocol(jsonProtocol) {
    
  }

  make(data, typeName) {
    const typeNotation = this.typeProtocol[typeName]
    if (!typeNotation) throw TypeError(`Type ${typeName} is undefined in the json protocol.`)
    _make(data, typeName, typeNotation)
  }

  _make(data, typeName, typeNotation) {
    if (data === null) {
      if (typeNotation === null) return null
      throw new TypeError(`Type ${typeName} is not expected to be of value null`)
    }
    switch (typeof typeNotation) {
      case 'boolean':
        return 
        break;
    
      default:
        break;
    }
  }
}

// let result = constructMessage(data, exampleJsonProtocol['add_client_msg'])
// console.log(result);