import {GenericTypeError, ConstantTypeError, UnionTypeError, ArrayTypeError, UnmatchArrayLengthError, ObjectTypeError ,MissingObjectPropertyError} from './error.js'

export default function validateMessage(data, typeName, jsonProtocol, emitError = false) {
  return checkType(data, typeName, jsonProtocol[typeName], jsonProtocol, emitError)
}

export function checkType(data, typeName, typeNotation, jsonProtocol, emitError = false) {
  const errorObj = emitError? {typeName} : null
  if (typeNotation === null) {
    return checkNull(data, errorObj)
  }
  switch (typeof typeNotation) {
    case 'boolean':
    case 'number':
      return checkConstant(data, typeNotation, errorObj)
    case 'string':
      switch (typeNotation) {
        case 'str':
          return checkString(data, errorObj)
        case 'bool':
          return checkBoolean(data, errorObj)
        case 'num':
          return checkNumber(data, errorObj)
        case 'obj':
          return checkObject(data, errorObj)
        case 'list':
          return checkArray(data, errorObj)
        default:
          /** list with specified length type */
          const listWithLength = typeNotation.match(/list\:(\d*)/)
          if (listWithLength) return checkArrayWithLength(data, parseInt(listWithLength[1]), errorObj)

          /** string constant type */
          const stringConstant = typeNotation.match(/const\:(.*)/)

          if (stringConstant) return checkString(data, errorObj) && checkConstant(data, stringConstant[1], errorObj)

          /** self-defined type */
          return checkSelfDefinedType(data, typeName, typeNotation, jsonProtocol, emitError)
      }
    case 'object':
      /** Union type */
      if (Array.isArray(typeNotation)) return checkUnion(data, typeName, typeNotation, jsonProtocol, emitError)

      for (const typeNameInObj in typeNotation) {
        const type = typeNotation[typeNameInObj]
        /** Typed array/list type */
        if (typeNameInObj === 'list') {
          return checkTypedArray(data, typeName, type, jsonProtocol, emitError)
        }
        /** Typed array/list with specified length type */
        const listWithLength = typeNameInObj.match(/list\:(\d*)/)
        if (listWithLength) {
          return checkArrayWithLength(data, parseInt(listWithLength[1]), errorObj) &&
          checkTypedArray(data, typeName, type, jsonProtocol, emitError)
        }
        break
      }

      /** Typed object type */
      return checkTypedObject(data, typeName, typeNotation, jsonProtocol, emitError)
  }
}

///////////////////////////
///    Generic types    ///
///////////////////////////

function checkGeneric(data, type, errorObj) {
  if (typeof data === type) return true
  if (!errorObj) return false
  throw new GenericTypeError(data, errorObj.typeName, type)
}

function checkString(data, errorObj) {
  return checkGeneric(data, 'string', errorObj)
}

function checkBoolean(data, errorObj) {
  return checkGeneric(data, 'boolean', errorObj)
}

function checkNumber(data, errorObj) {
  return checkGeneric(data, 'number', errorObj)
}

function checkObject(data, errorObj) {
  return checkGeneric(data, 'object', errorObj)
}

function checkArray(data, errorObj) {
  return checkGeneric(data, 'object', errorObj) && Array.isArray(data)
}

///////////////////////////
///    Constant type    ///
///////////////////////////

function checkConstant(data, val, errorObj) {
  if (data === val) return true
  if(!errorObj) return false
  throw new ConstantTypeError(data, errorObj.typeName, type)
}

function checkNull(data, errorObj) {
  return checkConstant(data, null, errorObj)
}

///////////////////////////
////    union type     ////
///////////////////////////

function checkUnion(data, typeName, typeList, jsonProtocol, emitError) {
  for (const type of typeList) {
    if (checkType(data, typeName, type, jsonProtocol, false)) return true
  }
  if (!emitError) return false
  throw new UnionTypeError(data, typeName, typeList)
}
///////////////////////////
////    object type    ////
///////////////////////////

function checkTypedObject(objectData, objectName, objectPropType, jsonProtocol, emitError) {
  return emitError? 
    checkTypedObjectWithError(objectData, objectName, objectPropType, jsonProtocol) :
    checkTypedObjectWithoutError(objectData, objectPropType, jsonProtocol)
}

function checkTypedObjectWithoutError(objectData, objectPropType, jsonProtocol) {
  for (const propType in objectPropType) {
    if (!objectPropType.hasOwnProperty(propType)) return false
    const propContent = objectData[propType]
    if(!checkType(propContent, propType, objectPropType[propType], jsonProtocol, false)) return false
  }
  return true
}

function checkTypedObjectWithError(objectData, objectName, objectPropType, jsonProtocol) {
  try {
    for (const propType in objectPropType) {
      // console.log(objectPropType);
      if (!objectPropType.hasOwnProperty(propType)) {
        throw new MissingObjectPropertyError(objectName, propType)
      }
      const propContent = objectData[propType]
      checkType(propContent, propType, objectPropType[propType], jsonProtocol, true)
    }
  } catch (error) {
    throw new ObjectTypeError(objectData, objectName, objectPropType, error)
  }
  return true
}

///////////////////////////
////     array type    ////
///////////////////////////

function checkArrayWithLength(data, length, errorObj) {
  return checkArray(data, errorObj) && checkArrayLength(data, length, errorObj)
}

function checkArrayLength(arrayData, length, errorObj) {
  if (arrayData.length === length) return true
  if (!errorObj) return false
  throw new UnmatchArrayLengthError(arrayData, errorObj.typeName, length)
}

function checkTypedArray(arrayData, arrayName, arrayType, jsonProtocol, emitError) {
  return emitError? 
    checkTypedArrayWithError(arrayData, arrayName, arrayType, jsonProtocol) :
    checkTypedArrayWithoutError(arrayData, arrayName, arrayType, jsonProtocol)
}

function checkTypedArrayWithoutError(arrayData, arrayName, arrayType, jsonProtocol) {
  for (const arrayItem of arrayData) {
    if (!checkType(arrayItem, arrayName, arrayType, jsonProtocol, false)) return false
  }
  return true
}

function checkTypedArrayWithError(arrayData, arrayName, arrayType, jsonProtocol) {
  let arrayIndex = 0
  try {
    for (const arrayItem of arrayData) {
      checkType(arrayItem, arrayName, arrayType, jsonProtocol, true)
      arrayIndex++
    } 
  } catch (error) {
    throw new ArrayTypeError(arrayData[arrayIndex], arrayName, arrayType, arrayIndex, error)
  }
  return true
}

///////////////////////////
//   self-defined type   //
///////////////////////////

function checkSelfDefinedType(data, typeName, typeNotation, jsonProtocol, emitError) {
  if (!jsonProtocol.hasOwnProperty(typeNotation)) {
    if (!emitError) return false
    throw new MissingObjectPropertyError(typeName, typeNotation)
  }
  return checkType(data, typeName, jsonProtocol[typeNotation], jsonProtocol, emitError)
}